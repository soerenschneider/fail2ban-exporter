""" Simple prometheus exporter for fail2ban """

import argparse
import logging
import os
import re
import subprocess
import sys
import time

from subprocess import CalledProcessError

from prometheus_client import CollectorRegistry, Counter, Gauge, generate_latest

REGISTRY = CollectorRegistry()
metrics = {
    'Currently banned:': Gauge('fail2ban_banned_current',
        'Number of currently banned IP addresses',
        ['jail'],
        registry=REGISTRY),

    'Total banned:': Gauge('fail2ban_banned_total',
        'Total number of banned IP addresses',
        ['jail'],
        registry=REGISTRY),

    'Currently failed:': Gauge('fail2ban_failed_current',
        'Number of currently failed ips',
        ['jail'],
        registry=REGISTRY),

    'Total failed:': Gauge('fail2ban_failed_total',
        'Total number of failed ips',
        ['jail'],
        registry=REGISTRY)
}

timestamp = Gauge('fail2ban_timestamp_seconds',
        'Timestamp of the latest export run',
        registry=REGISTRY)

errors = Counter('fail2ban_errors_total',
        'Errors during operation',
        ['error'],
        registry=REGISTRY)

FILE_NAME="fail2ban.prom"
REGEX = re.compile(r'('+ '|'.join(metrics.keys()) + r')\s*(\d*)')

class Fail2BanWrapper:
    """ Encapsulates the fail2ban-client program to provide greater testability. """
    CMD = "fail2ban-client"
    ARG = "status"

    def get_jails(self) -> str:
        """ Returns information about all jails as parsable text. """
        try:
            process = subprocess.Popen([self.CMD, self.ARG], stdout=subprocess.PIPE)
            return process.communicate()[0].decode('utf-8')
        except CalledProcessError:
            logging.error("Error while getting list of jails. Is fail2ban running?")
            errors.labels("list-jails").inc()
        except OSError as err:
            logging.error("Is fail2ban installed?: %s", err)

        return ""

    def get_status(self, jail: str) -> str:
        """ Returns status about all jails as parsable text. """
        try:
            process = subprocess.Popen([self.CMD, self.ARG, jail], stdout=subprocess.PIPE)
            return process.communicate()[0].decode('utf-8')
        except CalledProcessError:
            logging.error("Error while getting status for jail %s. Is fail2ban running?", jail)
            errors.labels(f"status-jail-{jail}").inc()
        except OSError as err:
            logging.error("Is fail2ban installed?: %s", err)


        return ""


class Fail2BanExporter:
    """ The exporter. """
    def __init__(self, location: str, jails=None, wrapper=None):
        if not location:
            raise "No location set"

        self.location = location

        if not wrapper:
            raise ValueError("No process backend")
        self.wrapper = wrapper

        self.jails = jails

    def loop(self) -> None:
        """ Loop until the program is terminated. """

        try:
            while True:
                if not self.jails:
                    self.jails = self.detect_jails()

                self.update()
                time.sleep(15)
        except KeyboardInterrupt:
            logging.info("Bye")
        except Exception as err: # pylint: disable=broad-except
            logging.error(err)

    def detect_jails(self) -> list:
        """ Parses jail text information and returns jails. """
        response = self.wrapper.get_jails()
        match = re.search(r'.+Jail list:\s+(.+)$', response)
        if not match:
            errors.labels("list-jails").inc()
            logging.error("Could not detect jails")
            return []

        jails = match.group(1).split(", ")
        logging.info("Detected %d jails: %s", len(jails), jails)
        return jails

    def update(self) -> None:
        """ Reads latest information and updates metrics. """
        for jail in self.jails:
            self.update_jail(jail)

        timestamp.set_to_current_time()
        with open(self.location, "wb") as prom_file:
            prom_file.write(generate_latest(REGISTRY))

    def update_jail(self, jail: str):
        """ Fetch and parse information for a particular jail. """

        response = self.wrapper.get_status(jail)
        if not response:
            logging.info("Ignoring empty response")
            return

        matches = re.findall(REGEX, response)
        if not matches:
            errors.labels("no-match").inc()
            return

        for match in matches:
            metrics[match[0]].labels(jail).set(float(match[1]))

def parse_args():
    """ Parse all cli args and returned Namespace object. """
    parser = argparse.ArgumentParser(description="fail2ban exporter")
    parser.add_argument('-j', '--jails', default=list(), nargs='+', help="Explicity state jails to export. Defaults to all detected jails.")
    parser.add_argument('-d', '--dir', default="/var/lib/node_exporter", help="Location of the node_exporter text directory")
    return parser.parse_args()

def main():
    """ Entrypoint """
    args = parse_args()

    if not os.path.isdir(args.dir):
        logging.error("Dir %s does not exist", args.dir)
        sys.exit(1)

    location = os.path.join(args.dir, FILE_NAME)
    fail2ban = Fail2BanWrapper()

    try:
        exporter = Fail2BanExporter(location=location, jails=args.jails, wrapper=fail2ban)
    except ValueError as err:
        logging.error(err)
        sys.exit(1)

    exporter.loop()

if __name__ == "__main__":
    main()
